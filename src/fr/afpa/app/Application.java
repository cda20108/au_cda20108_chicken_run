package fr.afpa.app;

import java.util.Scanner;

import fr.afpa.services.Gestion_Elevage;

public class Application {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		(new Gestion_Elevage(in)).menu();
		in.close();
	}
}
