package fr.afpa.services;

import java.util.Scanner;

import fr.afpa.beans.Abattable;
import fr.afpa.beans.Canard;
import fr.afpa.beans.Elevage;
import fr.afpa.beans.Paon;
import fr.afpa.beans.Poulet;
import fr.afpa.beans.Volaille;
import fr.afpa.utils.ControlDeSaisi;

public class Gestion_Elevage {

	private Elevage elevage;
	private Scanner in;

	public Gestion_Elevage(Scanner in) {
		this.elevage = new Elevage();
		this.in = in;
	}

	private void vendreUneVolaille() {
		String sID = "";
		do {
			System.out.print("Entrer l'id de volaille abattable � vendre");
			sID = in.next();
			in.nextLine();
		} while (!ControlDeSaisi.isInt(sID));
		Volaille volaille = elevage.rechercherVolailleAvecID(Integer.parseInt(sID));
		if (volaille != null) {
			if (volaille instanceof Abattable) {
				if (((Abattable) volaille).isAbatable()) {
					System.out.println(
							"ce " + volaille + " son prix est: " + ((Abattable) volaille).calculerPrix() + " euros.");
					elevage.enleverVolaille(volaille.getId());
					System.out.println("Bien vendu.");
				} else {
					System.err.println("Ce volaille n'a pas encore atteind le poids necessaire.");
				}
			} else {
				System.err.println("Ce n'est pas une volaille abattable.");
			}
		} else {
			System.err.println("Mauvais ID");
		}
	}

	private void afficherPrixTotalVolaillesAbattables() {
		System.out.println("Le prix total des volailles abattable est :"
				+ elevage.calculerPrixTotalVolaillesAbattables() + " euros.");
	}

	private void afficherNombreVolaillesParType() {
		String s = "";
		s += "Le nmbre des canards est : " + elevage.getNbCanards() + "\n";
		s += "Le nmbre des poulets est : " + elevage.getNbPoulets() + "\n";
		s += "Le nmbre des paons est : " + elevage.getNbPaons() + "\n";
		System.out.print(s);

	}

	private void rendreUnPaon() {
		if (elevage.getNbPaons() > 0) {
			String str = "";
			do {
				System.out.print("Entrer l'identifiant de paon a rendre >> ");
				str = in.next();
				in.nextLine();
			} while (!ControlDeSaisi.isInt(str));
			Volaille volaille = elevage.rechercherVolailleAvecID(Integer.parseInt(str));
			if (volaille != null && volaille instanceof Paon) {
				elevage.enleverVolaille(volaille.getId());
				System.out.println("Le " + volaille + " est bien rendu au parc.");
			} else {
				System.err.println("Mauvais ID");
			}
		} else {
			System.err.println("On n'a pas des paons dans notre elevage.");
		}
	}

	/**************************************************************************************************************/
	/****************************************
	 * Ajouter_une_Volaille
	 ****************************************/
	private void ajouterCanard() {
		if (!elevage.isNbCanardsMax()) {
			String sPoids = "";
			float poids = 0f;
			do {
				do {
					System.out.print("Entrer le poids de ce canard >> ");
					sPoids = in.next().replaceFirst(",", ".");
					in.nextLine();
				} while (!ControlDeSaisi.isFloat(sPoids));
				poids = Float.parseFloat(sPoids);
			} while (poids <= 0f);

			Canard canard = new Canard(poids);
			elevage.ajouterVolaille(canard);
			System.out.println("Le " + canard + " est bien ajouter.");

		} else {
			System.err.println("On ne peux pas ajouter plus de canards.");
		}
	}

	private void ajouterPoulet() {
		if (!elevage.isNbPouletsMax()) {
			String sPoids = "";
			float poids = 0f;
			do {
				do {
					System.out.print("Entrer le poids de ce poulet >> ");
					sPoids = in.next().replaceFirst(",", ".");
					in.nextLine();
				} while (!ControlDeSaisi.isFloat(sPoids));
				poids = Float.parseFloat(sPoids);
			} while (poids <= 0f);
			Poulet poulet = new Poulet(poids);
			elevage.ajouterVolaille(poulet);
			System.out.println("Le " + poulet + " est bien ajouter.");

		} else {
			System.err.println("On ne peux pas ajouter plus de poulets.");
		}
	}

	private void ajouterPaon() {
		if (!elevage.isNbPaonsMax()) {
			Paon paon = new Paon();
			elevage.ajouterVolaille(paon);
			System.out.println("Le " + paon + " est bient ajouter.");
		} else {
			System.err.println("On ne peux pas ajouter plus de paon.");
		}
	}

	private void ajouterUneVolaille() {
		if (!elevage.isNbVolaillesMax()) {
			String s = "";
			s += "Vous ajouter : \n";
			s += "	(1) Un canard.\n";
			s += "	(2) Un Poulet.\n";
			s += "	(3) Un paon.\n";
			s += "	(4) Sortir.";

			String choix = "";
			do {
				System.out.println(s);
				System.out.print("Entrer un choix >> ");
				choix = in.next();
				in.nextLine();
				switch (choix) {
				case "1":
					ajouterCanard();
					break;
				case "2":
					ajouterPoulet();
					break;
				case "3":
					ajouterPaon();
					break;
				case "4":
					System.out.println();
					break;
				default:
					System.err.println("Mauvais choix.");
					break;
				}
			} while (!"4".equals(choix));
		} else {
			System.err.println("D�sol�, On ne paut pas ajouter une nouvelle volaille.");
		}

	}

	private void modifierPoidsUneVolaille() {
		String sID = "";
		do {
			System.out.print("Entrer le ID de volaille a modifier son poids >> ");
			sID = in.next();
			in.nextLine();
		} while (!ControlDeSaisi.isInt(sID));
		Volaille volaille = elevage.rechercherVolailleAvecID(Integer.parseInt(sID));
		if (volaille != null && volaille instanceof Abattable) {
			System.out.println("Le " + volaille + ".");
			String sPoids = "";
			float poids = 0f;
			do {
				do {
					System.out.print("Entrer le nouveau poids >> ");
					sPoids = in.next().replaceFirst(",", ".");
					in.nextLine();
				} while (!ControlDeSaisi.isFloat(sPoids));
				poids = Float.parseFloat(sPoids);
			} while (poids <= 0f);
			volaille.setPoids(poids);
			System.out.println("Le poids a �t� bien mis � jour.");

		} else {
			System.err.println("Mauvais ID.");
		}
	}

	/**************************************************************************************************************/
	/***************************************
	 * Modification-poids_d'abattement
	 ****************************************/

	private void modifierPoidsAbatageCanards() {
		System.out.println("Le poid d'abatage actuel pour les canards est: " + Canard.getPoisDAbatement() + "kg.");
		String sPoids = "";
		Float poids = 0f;
		do {
			do {
				System.out.print("Entrer le nouveau poids >>");
				sPoids = in.next().replaceFirst(",", ".");
				in.nextLine();
			} while (!ControlDeSaisi.isFloat(sPoids));
			poids = Float.parseFloat(sPoids);
		} while (poids <= 0);
		Canard.setPoisDAbatement(poids);
		System.out.println("Le poids actuel est bien: " + poids + "kg");
	}

	private void modifierPoidsAbatagePoulets() {
		System.out.println("Le poid d'abatage actuel pour les poulets est: " + Poulet.getPoisDAbatement() + "kg.");
		String sPoids = "";
		Float poids = 0f;
		do {
			do {
				System.out.print("Entrer le nouveau poids >>");
				sPoids = in.next().replaceFirst(",", ".");
				in.nextLine();
			} while (!ControlDeSaisi.isFloat(sPoids));
			poids = Float.parseFloat(sPoids);
		} while (poids <= 0);
		Canard.setPoisDAbatement(poids);
		System.out.println("Le poids actuel est bien: " + poids + "kg.");
	}

	private void modifierPoidsAbatage() {
		String s = "";
		s += "Vous voulez modifier le pois d'abatage pour : \n";
		s += " (1) Les canards.\n";
		s += " (2) Les poulets.\n";
		s += " (3) sortir.";

		String choix = "";
		do {
			System.out.println(s);
			System.out.print("Entrer un choix >> ");
			choix = in.next();
			in.nextLine();
			switch (choix) {
			case "1":
				modifierPoidsAbatageCanards();
				break;
			case "2":
				modifierPoidsAbatagePoulets();
				break;
			case "3":
				System.out.println();
				break;
			default:
				System.err.println("Mauvais choix.");
				break;
			}
		} while (!"3".equals("3"));
	}

	/**************************************************************************************************************/
	/****************************************
	 * Modification-prix_de_jour
	 ****************************************/

	private void modifierPrixJourCanards() {
		System.out.println("Le prix de jour actuel pour les canards est: " + Canard.getPrix() + "euros.");
		String sPrix = "";
		Float prix = 0f;
		do {
			do {
				System.out.print("Entrer le nouveau prix >>");
				sPrix = in.next().replaceFirst(",", ".");
				in.nextLine();
			} while (!ControlDeSaisi.isFloat(sPrix));
			prix = Float.parseFloat(sPrix);
		} while (prix <= 0);
		Canard.setPrix(prix);
		System.out.println("Le prix actuel est bien: " + prix + "euros.");
	}

	private void modifierPrixJourPoulets() {
		System.out.println("Le prix de jour actuel pour les poulets est: " + Poulet.getPrix() + "euros.");
		String sPrix = "";
		Float prix = 0f;
		do {
			do {
				System.out.print("Entrer le nouveau prix >>");
				sPrix = in.next().replaceFirst(",", ".");
				in.nextLine();
			} while (!ControlDeSaisi.isFloat(sPrix));
			prix = Float.parseFloat(sPrix);
		} while (prix <= 0);
		Poulet.setPrix(prix);
		System.out.println("Le prix actuel est bien: " + prix + "euros.");
	}

	private void modifierPrixJour() {
		String s = "";
		s += "Vous voulez modifier le prix de jour pour : \n";
		s += " (1) Les canards.\n";
		s += " (2) Les poulets.\n";
		s += " (3) sortir.";

		String choix = "";
		do {
			System.out.println(s);
			System.out.print("Entrer un choix >> ");
			choix = in.next();
			in.nextLine();
			switch (choix) {
			case "1":
				modifierPrixJourCanards();
				break;
			case "2":
				modifierPrixJourPoulets();
				break;
			case "3":
				System.out.println();
				break;
			default:
				System.err.println("Mauvais choix.");
				break;
			}
		} while (!"3".equals("3"));
	}

	/**************************************************************************************************************/

	public void menu() {
		String s = "";
		s += "******************************************************************\n";
		s += "|                            <<<Menu>>>                          |\n";
		s += "|	<1> Ajouter une volaille.                                |\n";
		s += "|	<2> Modifier le poids d'abatage.                         |\n";
		s += "|	<3> Modifier le prix de jour.                            |\n";
		s += "|	<4> Modifier poids d'un volaille.                        |\n";
		s += "|	<5> Afficher le nombre de volailles par type.            |\n";
		s += "|	<6> Afficher le prix total des volailles abattables.     |\n";
		s += "|	<7> Vendre une volaille.                                 |\n";
		s += "|	<8> Rendre un paon au parc.                              |\n";
		s += "|	<9> Quit�.                                               |\n";
		s += "|	                                                         |\n";
		s += "******************************************************************";

		String choix = "";
		do {
			System.out.println(s);
			System.out.print("Entrer un choix >> ");
			choix = in.next();
			in.nextLine();
			switch (choix) {
			case "1":
				ajouterUneVolaille();
				;
				break;
			case "2":
				modifierPoidsAbatage();
				;
				break;
			case "3":
				modifierPrixJour();
				;
				break;
			case "4":
				modifierPoidsUneVolaille();
				;
				break;
			case "5":
				afficherNombreVolaillesParType();
				break;
			case "6":
				afficherPrixTotalVolaillesAbattables();
				break;
			case "7":
				vendreUneVolaille();
				break;
			case "8":
				rendreUnPaon();
				break;
			case "9":
				System.out.println("Au revoire.");
				break;
			default:
				System.err.println("Mauvais choix.");
				break;
			}
		} while (!"9".equals(choix));

	}

}
