package fr.afpa.utils;

public final class ControlDeSaisi {

	public static boolean isInt(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isFloat(String str) {
		boolean point = false;
		char c;
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (c == '.' && i != 0 && !point) {
				point = true;
			} else if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isAlphaBetique(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isAlphabetic(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isAlphaNumerique(String str) {
		char c;
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (!(Character.isAlphabetic(c) || Character.isDigit(c))) {
				return false;
			}
		}
		return true;
	}

}
