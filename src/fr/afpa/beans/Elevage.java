package fr.afpa.beans;

public class Elevage {
	private final int nbCanardsMax = 4;//LE NOMBRE MAX DE CANAREDS
	private final int nbPouletsMax = 5;//LE NOMBRE MAX DE POULETS
	private final int nbPaonsMax = 3;//LE NOMBRE MAX DE PAONS

	private int nbCanards;//LE NOMBRE ACTUEL DE CANARDS
	private int nbPoulets;//LE NOMBRE ACTUEL DE POULETS
	private int nbPaons;//LE NOMBRE ACTUEL DE PAONS

	private int nbVolailles;//LE NOMBRE TOTAL DE VOLAILLES
	private Volaille[] volaillesTab;

	public Elevage() {
		this.nbVolailles = 0;
		this.volaillesTab = new Volaille[7];
		nbCanards = 0;
		nbPoulets = 0;
		nbPaons = 0;
	}

	public int getNbVolailles() {
		return nbVolailles;
	}

	public Volaille[] getVolaillesTab() {
		return volaillesTab;
	}

	public boolean isNbCanardsMax() {/** pour verifier si on atteint le nombre maximom de Canard **/
		return nbCanards == nbCanardsMax;
	}

	public boolean isNbPouletsMax() {/** pour verifier si on atteint le nombre maximom de Poulet **/
		return nbPoulets == nbPouletsMax;
	}

	public boolean isNbPaonsMax() {/** pour verifier si on atteint le nombre maximom de Paon **/
		return nbPaons == nbPaonsMax;
	}

	public boolean isNbVolaillesMax() {/** pour verifier si le tableau est complet ou pas **/
		return nbVolailles == volaillesTab.length;
	}

	public int getNbCanards() {
		return nbCanards;
	}

	public int getNbPoulets() {
		return nbPoulets;
	}

	public int getNbPaons() {
		return nbPaons;
	}

	public void ajouterVolaille(Volaille volaille) {/** pour ajouter une volaille de tableau volaillesTab **/
		volaillesTab[nbVolailles++] = volaille;
		if(volaille instanceof Canard) {
			nbCanards++;
		}
		else if(volaille instanceof Poulet) {
			nbPoulets++;
		}
		else {
			nbPaons++;
		}
	}

	public void enleverVolaille(int id) {/** pour enlever une volaille de tableau volaillesTab **/
		int index = -1;
		for (int i = 0; i < nbVolailles; i++) {
			if (volaillesTab[i].getId() == id) {
				index = i;
				break;
			}
		}
		for (int i = index; i < nbVolailles - 1; i++) {
			volaillesTab[i] = volaillesTab[i + 1];
		}
		volaillesTab[--nbVolailles] = null;
		
	}

	public Volaille rechercherVolailleAvecID(int id) {
		for (int i = 0; i < nbVolailles; i++) {
			if (volaillesTab[i].getId() == id) {
				return volaillesTab[i];
			}
		}
		return null;
	}

	public float calculerPrixTotalVolaillesAbattables() {
		float total = 0f;
		Volaille volaille = null;
		for (int i = 0; i < nbVolailles; i++) {
			volaille = volaillesTab[i];
			if (volaille instanceof Abattable && ((Abattable) volaille).isAbatable()) {
				total += ((Abattable) volaille).calculerPrix();
			}
		}
		return total;
	}

}
