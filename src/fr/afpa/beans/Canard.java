package fr.afpa.beans;

public final class Canard extends Volaille implements Abattable {

	private static float prix;
	private static float poisDAbatement;

	public Canard(float poids) {
		super(poids);
	}

	public static float getPrix() {
		return prix;
	}

	public static void setPrix(float prix) {
		Canard.prix = prix;
	}

	public static float getPoisDAbatement() {
		return poisDAbatement;
	}

	public static void setPoisDAbatement(float poisDAbatement) {
		Canard.poisDAbatement = poisDAbatement;
	}

	@Override
	public boolean isAbatable() {
		return super.getPoids() >= Canard.poisDAbatement;
	}

	@Override
	public float calculerPrix() {
		return super.getPoids() * prix;
	}

	@Override
	public String toString() {
		return "Canard id: " + super.getId() + " son poids: " + super.getPoids() + "kg";
	}

}
