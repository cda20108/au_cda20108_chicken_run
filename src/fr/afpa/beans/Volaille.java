package fr.afpa.beans;

public abstract class Volaille {
	private static int count;
	private int id;
	private float poids;

	public Volaille(float poids) {
		this.id = count++;
		this.poids = poids;
	}

	public final int getId() {
		return id;
	}

	public final void setId(int id) {
		this.id = id;
	}

	public final float getPoids() {
		return poids;
	}

	public final void setPoids(float poids) {
		this.poids = poids;
	}

	public abstract String toString();

}
