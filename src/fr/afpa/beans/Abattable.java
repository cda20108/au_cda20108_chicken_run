package fr.afpa.beans;

public interface Abattable {
	
	boolean isAbatable();
	
	float calculerPrix();

}
