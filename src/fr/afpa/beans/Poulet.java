package fr.afpa.beans;

public class Poulet extends Volaille implements Abattable {

	private static float prix;
	private static float poisDAbatement;

	public Poulet(float poids) {
		super(poids);
	}

	@Override
	public boolean isAbatable() {
		return super.getPoids() >= Poulet.poisDAbatement;
	}

	public static float getPrix() {
		return prix;
	}

	public static void setPrix(float prix) {
		Poulet.prix = prix;
	}

	public static float getPoisDAbatement() {
		return poisDAbatement;
	}

	public static void setPoisDAbatement(float poisDAbatement) {
		Poulet.poisDAbatement = poisDAbatement;
	}

	@Override
	public float calculerPrix() {
		return super.getPoids() * prix;
	}

	@Override
	public String toString() {
		return "Poulet id: " + super.getId() + " le poids: " + getPoids() + "kg";
	}

}
